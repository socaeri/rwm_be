from __future__ import unicode_literals
from djongo import models
import djongo
class team(models.Model):
    batch_year = models.IntegerField(db_column='Batch Year')  # Field name made lowercase.
    # roll_nos = djongo.models.ListField(db_column='Roll Nos',default=True)  # Field name made lowercase.
    # student_name = djongo.models.ListField(db_column='Student Names',default=True)  # Field name made lowercase.
    project_name = models.CharField(max_length=250, db_column='Project Name',default=True)  # Field name made lowercase.
    project_supervisor = models.CharField(max_length=250, db_column='Project Supervisor',default=True)  # Field name made lowercase.
    external_supervisor = models.CharField(max_length=250,db_column='External Supervisor',default=True)  # Field name made lowercase.
    co_supervisor = models.CharField(max_length=250,db_column='Co-Supervisor',default=True)  # Field name made lowercase.